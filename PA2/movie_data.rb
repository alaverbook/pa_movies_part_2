# movie_data.rb
# define class MovieData
class MovieData

	#updated initialize to accept a path and base_set, and set defaults if they are not given
	def initialize(data_path="ml-100k", base_set="u.data")
		@user_to_movies_and_ratings = Hash.new
		@movie_to_users_and_ratings = Hash.new
		@movie_ids_to_popularity = Hash.new
		@sorted_popularity_list = []
		@user1_user2_to_similarity = Hash.new
		@test_set = []
		if base_set.to_s == "u.data"
			load_data(data_path, base_set)
		else
			test_set = base_set.to_s + ".test"
			base_set = base_set.to_s + ".base"
			load_data(data_path, base_set)
			load_test_data(data_path, test_set)
		end
	end

	attr_accessor :user_to_movies_and_ratings, :movie_to_users_and_ratings,
		:movie_ids_to_popularity, :sorted_popularity_list, :user1_user2_to_similarity,
		:test_set
	
	#load_data accepts a path and base set and now loads the reviews into hashes
	# one where the movie_ids are keys and the other where the user_ids are keys
	def load_data(data_path, base_set)
		#read in the data from original ml-100k files and
		#store them in whichever way it needs to be stored
		file = data_path.to_s + "/" + base_set.to_s
		txt = open(file, "r").each_line do |line|
			data = line.split(/\t/)
			key = data[0].chomp
			value = data[1].chomp + ":" + data[2].chomp
			if @user_to_movies_and_ratings.has_key?(key)
				value = @user_to_movies_and_ratings[key] + "_" + value
			end
			@user_to_movies_and_ratings.store(key, value)

			key = data[1].chomp
			value = data[0].chomp + ":" + data[2].chomp
			if @movie_to_users_and_ratings.has_key?(key)
				value = @movie_to_users_and_ratings[key] + "_" + value
			end
			@movie_to_users_and_ratings.store(key, value)
		end
	end
	
	#popularity was updated to function with the hashes
	def popularity(movie_id)
		#return a number that indicates the popularity (higher = most)
		#reason my definition of popularity
		if !@movie_to_users_and_ratings.has_key?(movie_id)
			return -1
		end
		if @movie_ids_to_popularity.has_key?(movie_id)
			return @movie_ids_to_popularity[movie_id]
		end
		sum = 0
		reviews = @movie_to_users_and_ratings[movie_id].split("_")
		reviews.each do |review|
			sum += 1
		end
		pop = (((sum * 1.0) / @user_to_movies_and_ratings.size) * 10000).round / 100.0
		@movie_ids_to_popularity.store(movie_id, pop)
		return pop
	end
	
	#popularity list is now much more efficient by accessing a hash instead of an array
	def popularity_list
		#generate a list of all movie-id's ordered by decreasing popularity
		i = 1
		while i <= @movie_to_users_and_ratings.size
			popularity(i.to_s)
			i += 1
		end
		sorted_popularity_list = @movie_ids_to_popularity.sort_by { |key, value| value}
		@sorted_popularity_list = sorted_popularity_list.reverse
	end

	#I generalized printing the first and last ten most ___ into its own method
	def print_first_and_last_ten_most(array, list)
		first_and_last_ten = []
		first_and_last_ten.push(array.take(10))
		last_ten = array.pop(10)
		array.push(last_ten)
		first_and_last_ten.push(last_ten)

		puts "The 10 most " + list + " are:"
		first_and_last_ten[0].each do |tuple|
			print "\t" + tuple[0].to_s + "\t" + tuple[1].to_s + "\n"
		end
		puts "The 10 least " + list + " are:"
		first_and_last_ten[1].each do |tuple|
			print "\t" + tuple[0].to_s + "\t" + tuple[1].to_s + "\n"
		end
	end
	
	#similarity need only compare the movies seen by both users so it accesses the user hash
	#it also now takes into account a penalty if the users had different opinions about a movie
	def similarity(user1, user2)
		#generate a number which indicates the similarity in movie preference
		#user1 and user2 where higher numbers indicate greater similarity
		user1 = user1.to_s
		user2 = user2.to_s
		key1 = user1 + ":" + user2
		key2 = user2 + ":" + user1
		if @user1_user2_to_similarity.has_key?(key1)
			return @user1_user2_to_similarity[key1]
		end
		count_both_seen = 0
		count_similar_rating = 0
		count_user1_total_movies = 0
		user1_reviews = @user_to_movies_and_ratings[user1].split("_")
		user2_reviews = @user_to_movies_and_ratings[user2].split("_")
		user1_reviews.each do |user1_review_pair|
			count_user1_total_movies += 1
			user2_reviews.each do |user2_review_pair|
				user1_review = user1_review_pair.split(":")
				user2_review = user2_review_pair.split(":")
				if user1_review[0] == user2_review[0]
					count_both_seen += 1
					if user1_review[1].to_i == user2_review[1].to_i
						count_similar_rating += 5
					elsif user1_review[1].to_i > user2_review[1].to_i
						count_similar_rating += (5 - (user1_review[1].to_i - user2_review[1].to_i))
					else
						count_similar_rating += (5 - (user2_review[1].to_i - user1_review[1].to_i))
					end
				end
			end
		end
		key1 = user1 + ":" + user2
		key2 = user2 + ":" + user1
		if count_both_seen == 0
			@user1_user2_to_similarity.store(key1, 0)
			@user1_user2_to_similarity.store(key2, 0)
			return 0
		end
		result = (count_similar_rating * 1.0 / (count_both_seen * 5.0) * 10000.0).round / 100.0
		result = result * ((count_both_seen * 1.0) / count_user1_total_movies)
		return result
	end
	
	#this is more efficient since it now is dealing with the hashes
	def most_similar(u)
		#return a list of users whose tastes are most similar to the tastes of user u
		similarity_list_for_u = Hash.new
		@user_to_movies_and_ratings.each_key {|key|
			if u.to_s != key
				similarity_list_for_u.store(key, similarity(u, key))
			end}
		sorted_similarity_list = similarity_list_for_u.sort_by { |key, value| value}
		sorted_similarity_list = sorted_similarity_list.reverse
		return sorted_similarity_list
	end

	#this loads the test data into test_set which is an array to be used for run_test
	def load_test_data(data_path, test_set)
		#read in the data from original ml-100k files and
		#store them in whichever way it needs to be stored
		file = data_path.to_s + "/" + test_set.to_s
		txt = open(file, "r").each_line do |line|
			data = line.split(/\t/)
			@test_set.push(data[0].chomp + ":" + data[1].chomp + ":" + data[2].chomp)
		end
	end

	#this looks up the rating in the base set if the user provided a rating
	#it returns -1 if no rating was provided by the user for the movie
	def rating(user_id, movie_id)
		if @user_to_movies_and_ratings.has_key?(user_id)
			reviews = @user_to_movies_and_ratings[user_id].split("_")
			reviews.each do |review|
				pair = review.split(":")
				if pair[0] == movie_id.to_s
					return pair[1].to_i
				end
			end
		end
		return -1
	end

	#predict finds the similarity to all other users who have seen at least one movie that user_id
	#has seen. It then looks for the ratings the most similar users have given to movie_id
	#and calculates an average of the ratings for all the users that provided a rating.
	def predict(user_id, movie_id)
		similar_users = Hash.new
		movies_seen = movies(user_id)
		movies_seen.each do |movie|
			other_users = viewers(movie)
			other_users.each do |user|
				if (user.to_s != user_id.to_s) && !similar_users.has_key?(user)
					similarity = similarity(user_id, user)
					similar_users.store(user, similarity)
				end
			end
		end
		sorted_similarity_list = similar_users.sort_by { |key, value| value}
		sorted_similarity_list = sorted_similarity_list.reverse
		ratings = 0
		count = 0
		max = 0
		sorted_similarity_list.each do |user_similarity|
			max = user_similarity[1]
			if (user_similarity[1].to_f / max) >= 0.90
				similar_user_movies = movies(user_similarity[0].to_s)
				similar_user_movies.each do |similar_movie|
					if similar_movie.to_s == movie_id
						rating = rating(user_similarity[0].to_s, movie_id.to_s)
						if rating >= 0
							ratings += rating
							count += 1
						end
					end
				end
			end
		end
		if count != 0
			return (((ratings * 1.0) / count) * 10.0).round / 10.0
		end
		return (-1).to_f
	end

	#accesses the user hash to return all the movies a user has seen
	def movies(user_id)
		movies = []
		if @user_to_movies_and_ratings.has_key?(user_id)
			reviews = @user_to_movies_and_ratings[user_id].split("_")
			reviews.each do |review|
				pair = review.split(":")
				movies.push(pair[0].to_s)
			end
		end
		return movies
	end

	#accesses the movie hash to return all the users who have seen a movie
	def viewers(movie_id)
		users = []
		if @movie_to_users_and_ratings.has_key?(movie_id)
			reviews = @movie_to_users_and_ratings[movie_id].split("_")
			reviews.each do |review|
				pair = review.split(":")
				users.push(pair[0].to_s)
			end
		end
		return users
	end

	#runs predict n times and returns a MovieTest object
	def run_test(n=@test_set.size)
		i = 0
		predictions = Hash.new
		while i < n.to_i
			line = @test_set[i]
			review = line.split(":")
			prediction = predict(review[0].to_s, review[1].to_s)
			puts Time.now
			i += 1
			predictions.store(line, prediction)
		end
		mt = MovieTest.new(predictions)
		return mt
	end
end

#define class MovieTest
class MovieTest

	#takes the hash of predictions from run_test
	def initialize(predictions)
		@predictions = predictions.sort_by { |key, value| value}
	end

	#sums the error for each prediction and divides by the number of predictions
	def mean
		sum_error = 0.0
		@predictions.each do |tuple|
			sum_error += error(tuple[0], tuple[1]).to_f
		end
		return (sum_error / @predictions.size)
	end

	#calculates the error of a review and prediction and returns the error as a percent
	def error(review, prediction)
		review = review.split(":")
		error = 0
		if review[2].to_f >= prediction.to_f
			error = review[2].to_f - prediction.to_f
		else
			error = prediction.to_f - review[2].to_f
		end
		return (error / 4.0) * 100
	end

	#calculates the standard deviation as a percent
	def stddev
		sum = 0
		mean_error = mean()
		@predictions.each do |tuple|
			error = error(tuple[0], tuple[1]).to_f - mean_error
			error = error * error
			sum += error
		end
		result = sum / @predictions.size
		result = Math.sqrt(result)
		return result
	end

	#calculates the root mean square error as a percent
	def rms
		sum = 0
		mean_error = mean()
		@predictions.each do |tuple|
			review = tuple[0].split(":")
			error = error(tuple[0], tuple[1]).to_f - review[2].to_f
			error = error * error
			sum += error
		end
		result = sum / @predictions.size
		result = Math.sqrt(result)
		return result
	end

	#converts the hash into an array in the format as requested by the assignment
	def to_a
		result = []
		@predictions.each do |tuple|
			review = tuple[0].split(":")
			temp = [review[0].to_i, review[1].to_i, review[2].to_f, tuple[1].to_f]
			result.push(temp)
		end
		return result
	end
end


md = MovieData.new
# md.popularity_list
# md.print_first_and_last_ten_most(md.sorted_popularity_list, "popular movies")
# sorted_similarity_list = md.most_similar("1")
# md.print_first_and_last_ten_most(sorted_similarity_list, "similar users to user 1")
md = MovieData.new("ml-100k")
md = MovieData.new("ml-100k", :u1)
mt = md.run_test(20)
puts "\nmean: " + mt.mean.to_s + "%\n"
puts "stddev: " + mt.stddev.to_s + "%\n"
puts "rms: " + mt.rms.to_s + "%\n"
puts "to_a: " + mt.to_a.to_s + "\n"

